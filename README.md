# Small flask app and docker

## Follow the above steps in order to get the app working

### Step 1
You need to have Docker installed. This is the only requirement 

### Step 2 
Download the flaskapp.tar dir. Then go to the download path with a terminal

### Step 3
Enter the following command: "docker load < flaskapp.tar"
You can check if it worked by typing the command "docker images". It should appear in your images

Note: Sometimes you have to run this command instead if the first didn't work: docker load -i flaskapp.tar

### Step 4
Run the following command: "docker run -it -d -p 5000:5000 flaskapp"
You can check if it worked by typing the command "docker ps"

### Step 5
Congrats ! You just have to open your browser and go the following address: 127.0.0.1:5000/
You now have access to the app
