from flask import Flask, render_template, redirect, url_for
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import SQLAlchemyError
from flask_login import UserMixin, login_user, LoginManager, login_required, current_user, logout_user
from flask_bootstrap import Bootstrap
from werkzeug.security import generate_password_hash, check_password_hash
from wtforms.validators import DataRequired, Email
import os


app = Flask(__name__)
app.config['SECRET_KEY'] = "8BYkEfBA6O6donzWlSihBXox7C0sKR6b"
Bootstrap(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///user.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))


if 'user.db' not in os.listdir():
    db.create_all()


class AddUser(FlaskForm):
    email = StringField(label="Adresse mail", validators=[DataRequired(), Email("Adresse mail invalide")])
    password = PasswordField(label="Mot de passe", validators=[DataRequired(), Email("Mot de passe invalide")])
    submit = SubmitField(label="Envoyer")


class ChangeMail(FlaskForm):
    email = StringField(label="Votre nouvelle adresse mail",  validators=[DataRequired(),
                                                                          Email("Adresse mail invalide")])
    submit = SubmitField(label="Enregistrer")


login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    try:
        return User.query.get(int(user_id))
    except ValueError:
        pass


class UserForm(FlaskForm):
    email = StringField(label="Adresse mail")
    password = PasswordField(label="Mot de passe")
    submit = SubmitField(label="Envoyer")


@app.route('/')
def home():
    logged_in = True if current_user.is_authenticated else False
    return render_template("index.html", logged_in=logged_in)


@app.route("/login", methods=["GET", "POST"])
def login():
    form = UserForm()
    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data
        user = User.query.filter_by(email=email).first()
        try:
            if check_password_hash(user.password, password):
                login_user(user)
        except AttributeError:
            return render_template("login.html", form=form, error_msg="Votre email/password ne correspond pas")
        else:
            return redirect(url_for('user_info'))

    return render_template("login.html", form=form)


@app.route('/register', methods=["GET", "POST"])
def register():
    form = UserForm()
    if form.validate_on_submit():
        hash_and_salted_password = generate_password_hash(
            form.password.data,
            method='pbkdf2:sha256',
            salt_length=8
        )
        new_user = User(
            email=form.email.data,
            password=hash_and_salted_password,
        )
        try:
            db.session.add(new_user)
            db.session.commit()
        except SQLAlchemyError:
            print("User already exists")

        login_user(new_user)

        return redirect(url_for("user_info"))

    return render_template("register.html", form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route("/user-info", methods=['GET', 'POST'])
@login_required
def user_info():
    form = ChangeMail()
    if form.validate_on_submit():
        user = User.query.filter_by(email=current_user.email).first()
        user.email = form.email.data
        db.session.commit()
        return redirect(url_for("user_info"))
    return render_template("user-info.html", email=current_user.email, form=form)


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5000)
